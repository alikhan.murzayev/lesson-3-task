package main

import "gitlab.com/alikhan.murzayev/lesson-3-module/deck"

func main() {
	deck := deck.Deck{}
	deck.Init()
	deck.Shuffle()
	deck.Print()

}
