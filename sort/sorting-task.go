package main

import (
	"fmt"
	"gitlab.com/alikhan.murzayev/lesson-3-module/bubblesort"
	"reflect"
	"sort"
	"strconv"
	"time"
)

func main() {
	maxValue := 10000

	for size := 10; size <= 100000; size += size / 2 {
		// for bubble sorting
		sliceBubble := *bubblesort.GenerateRandomSlice(size, maxValue)
		// for built-in sorting
		sliceStandard := make([]int, size)
		copy(sliceStandard, sliceBubble)
		// for quick sorting
		sliceQuick := make([]int, size)
		copy(sliceQuick, sliceBubble)

		// measure time that built-in sorting takes
		standardStartTime := time.Now()
		sort.Slice(sliceStandard, func(i, j int) bool {
			return sliceStandard[i] < sliceStandard[j]
		})
		standardEndTime := time.Now()
		standardElapsedTime := standardEndTime.Sub(standardStartTime)

		// measure time that bubble sorting takes
		bubbleStartTime := time.Now()
		bubblesort.BubbleSort(&sliceBubble)
		bubbleEndTime := time.Now()
		bubbleElapsedTime := bubbleEndTime.Sub(bubbleStartTime)

		// measure time that quick sorting takes
		quickStartTime := time.Now()
		bubblesort.QuickSort(&sliceQuick, 0, size-1)
		quickEndTime := time.Now()
		quickElapsedTime := quickEndTime.Sub(quickStartTime)

		fmt.Println("----------------------")
		if reflect.DeepEqual(sliceBubble, sliceStandard) && reflect.DeepEqual(sliceQuick, sliceStandard) {
			fmt.Println("Sorted correctly.")
		} else {
			fmt.Println("Sorted incorrectly.")
		}
		fmt.Println(strconv.Itoa(size), "elements")
		fmt.Println("Standard sorting time :", standardElapsedTime)
		fmt.Println("Quick sorting time    :", quickElapsedTime)
		fmt.Println("Bubble sorting time   :", bubbleElapsedTime)
	}
}
